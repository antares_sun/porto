Rails.application.routes.draw do
  resources :documents
  resources :pages
  resources :portfolios

  # get 'signup', to: 'users#new', as: 'signup' //For Admin only
  get 'login', to: 'sessions#new', as: 'login'
  delete 'logout', to: 'sessions#destroy', as: 'logout'

  resources :users
  resources :sessions
  
  namespace :admin do
    get '', to: 'dashboard#index', as: '/'
    resources :portfolios
    resources :users
    resources :documents
  end
  
  root to: "pages#home"
end
