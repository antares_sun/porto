class DocumentsController < ApplicationController
  before_action :set_document, only: [:show]

  def index
    @documents = Document.all
  end

  def show
    @document = Document.find(params[:id])
    send_data(@document.file_contents,
              type: @document.content_type,
              filename: @document.filename,
              :disposition => 'inline')
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end
end
