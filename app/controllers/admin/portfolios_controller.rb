class Admin::PortfoliosController < ApplicationController
  before_action :set_portfolio, only: [:show, :edit, :update, :destroy]
  before_filter :authorize

  layout "admin"

  def index
    # @portfolios = Portfolio.all
    # @portfolios = Portfolio.joins(:documents)
    @portfolios = Portfolio.joins('INNER JOIN documents ON documents.portfolio_id = portfolios.id')
  end

  def show
  end

  def new
    @portfolio = Portfolio.new
  end

  def edit
  end

  def create
    @portfolio = Portfolio.create(portfolio_params)
    @portfolio.documents.create(document_params)
    
    respond_to do |format|
      if @portfolio.save
        format.html { redirect_to admin_portfolios_url, notice: 'Portfolio was successfully created.' }
        format.json { render :show, status: :created, location: [:admin, @portfolio] }
      else
        format.html { render :new }
        format.json { render json: @portfolio.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @portfolio.update(portfolio_params)
        format.html { redirect_to admin_portfolios_url, notice: 'Portfolio was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @portfolio] }
      else
        format.html { render :edit }
        format.json { render json: @portfolio.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @portfolio.destroy
    respond_to do |format|
      format.html { redirect_to admin_portfolios_url, notice: 'Portfolio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_portfolio
      @portfolio = Portfolio.find(params[:id])
    end

    def portfolio_params
      params.require(:portfolio).permit(:name, :describtion, documents_attributes: [:id, :file])
    end

    def document_params
      params.require(:portfolio).permit(:file)
    end
end