class Admin::DocumentsController < ApplicationController
  before_action :set_document, only: [:show, :edit, :update, :destroy]
  before_filter :authorize

  layout "admin"

  def index
    @documents = Document.all
  end

  def show
#    @document = Document.find(params[:id])
    send_data(@document.file_contents,
              type: @document.content_type,
              filename: @document.filename,
              :disposition => 'inline')
  end

  def new
    @document = Document.new
  end

  def create
    @document = Document.new(document_params)
    
    @document.render

    respond_to do |format|
      if @document.save
        format.html { redirect_to admin_documents_url, notice: 'Document was successfully created.' }
        format.json { render action: 'show', status: :created, location: [:admin, @document] }
      else
        format.html { render action: 'new' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end
    
  def edit
  end

  def update
    respond_to do |format|
      if @document.update(document_params)
        format.html { redirect_to admin_documents_url, notice: 'Document was successfully updated.' }
        format.json { render :show, status: :ok, location: [:admin, @document] }
      else
        format.html { render action: 'edit' }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @document.destroy
    respond_to do |format|
      format.html { redirect_to admin_documents_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_document
      @document = Document.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def document_params
        params.require(:document).permit(:file)
    end
end
