class Portfolio < ActiveRecord::Base
  has_many :documents, autosave: true, :dependent => :destroy, foreign_key: "portfolio_id"
  
  accepts_nested_attributes_for :documents, allow_destroy: true
end
