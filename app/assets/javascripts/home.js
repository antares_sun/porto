$(function(){
  var global = (function(){
    var classes = function(selector, add, remove){
      selector.removeClass(remove).addClass(add);
    };

    return {
      classes: classes
    }
  })();

  var header = (function(){
    // Header Global Variable
    var $mainMenu = $('ul.main-menu'),
        $li = $mainMenu.find('li'),
        delayTimeIn = [1, 0.5, 0];
        delayTimeOut = [0, 0.5, 1];

    var clickMenu = function(){
      // Local Variable for function clickMenu
      var $mainMenu = $('.icon-menu'),
          $navMenu = $('.menu');
      
      $mainMenu.click(function(){
        if($navMenu.hasClass('not-active')){
          _slideIn(); 
          global.classes($navMenu, 'active', 'not-active');
        } else {
          _slideOut();
          global.classes($navMenu, 'not-active', 'active');
        }                 
      });
    },
    _getChildAnimation = function(number, time){
      $mainMenu.find('li:nth-child(' + number + ')').css({
        'animation-delay': '' + time + 's'
      });
    },
    _slideIn = function(){
      $mainMenu.show();
      global.classes($li, 'slideInUp', 'slideOutDown');

      $.each(delayTimeIn, function(i, delay){
        _getChildAnimation((i+1), delay);
      });
    },
    _slideOut = function(){
      setTimeout(function(){
        $mainMenu.hide();
      }, 1400);
      global.classes($li, 'slideOutDown', 'slideInUp');
      
      $.each(delayTimeOut, function(i, delay){
        _getChildAnimation((i+1), delay);
      });
    },
    notice = function() {
      var $notice = $('#notice');
      
      if($notice.text() !== ""){
        $notice.fadeIn().delay(3000).fadeOut();
      } else {
        $notice.fadeOut();
      }
    };

    return {
      clickMenu: clickMenu,
      notice: notice
    }
  })();

  var content = (function(){
    var clickMe = function(element){
      var $me = $('#myself');

      $me.click(function(){
        $('.arrow-down, .line, #close-me').animate({
          opacity: 1
        }, 1000);
        $('.profile > img').css({
          top: 0,
          opacity: 1
        });

        global.classes($me, 'active-me', 'not-active-me');

        $('#close-me').show();

        $(this).css({
          background: '#333'
        });

        $me.children('p').text("");
      });

      $(document).on('click', '#close-me', function(){
        $('.arrow-down, .line, #close-me').animate({
          opacity: 0
        }, 1000);
        $('.profile > img').css({
          top: '100%'
        });
        setTimeout(function(){
          global.classes($me, 'not-active-me', 'active-me');
          $me.css({
            background: 'transparent'
          });
          $me.children('p').text("Me");
        }, 1000);
      });
      var meHeight = $me.offset().top;
      $(element).css(
        'height', meHeight
      );
    },
    bodyClick = function(){
      // $('body').css('color', 'red');
    },
    _fadeIn = function(el){
      $(el).animate({opacity: 1}, 500);
    },
    _fadeOut = function(el){
      $(el).animate({opacity: 0}, 500);
    };

    return {
      clickMe: clickMe,
      bodyClick: bodyClick
    }
  })();

  // Run javascript on specific page only
  var route = {
    _routes: {},

    add: function(url, action){
      this._routes[url] = action;
    },
    run: function(){
      $.each(this._routes, function(pattern){
        if(location.pathname.match(pattern)){
          this();
        }
      });
    }
  };

  // All Pages
  route.add('/', function(){
    header.notice();
  });
  
  route.add('portfolios', function(){
    content.bodyClick();
  });
  route.add('^/$', function(){
    header.clickMenu();
    content.clickMe('.profile');
  });

  route.run();
});