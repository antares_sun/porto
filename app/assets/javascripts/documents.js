$(function(){
  var content = (function(){
    var display_image = function(){
        $('.photo_upload').on('change', function(){
          _previewImage();
        });
    },
    _previewImage = function previewImage(){
      var $preview = $('.image_to_upload'),
          $submit = $('input[type=submit]'),
          file    = document.querySelector('input[type=file]').files[0];

      if ( /\.(jpe?g|png|gif)$/i.test(file.name) ) {
        var reader  = new FileReader();

        reader.onload = function(event){
          $preview.attr('src', event.target.result);

          return false;
        };
        if(file){
          reader.readAsDataURL(file);
        }

        $submit.removeAttr('disabled').removeClass('norun');
      } else {
        alert('Only jpg, jpeg, png, or gif filetype.');
        $submit.attr('disabled', 'disabled').addClass('norun');

        $submit.click(function(e){
          if($submit.hasClass('norun')){
            e.preventDefault();
          }
        });
      }
    };

    return {
      display_image: display_image
    }
  })();

  content.display_image();
});