class CreatePortfolios < ActiveRecord::Migration
  def change
    create_table :portfolios do |t|
      t.string :name
      t.string :describtion

      t.timestamps null: false
    end
  end

  # def up
  #   change_table :products do |t|
  #     t.change :price, :string
  #   end
  # end
 
  # def down
  #   change_table :products do |t|
  #     t.change :price, :integer
  #   end
  # end
end
