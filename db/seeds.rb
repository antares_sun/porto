# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Portfolio.create!([
  {
    :name   => 'Leegea TV',
    :describtion    => 'Magazine Web TV'
  },
  {
    :name   => 'IS-Comp Management',
    :describtion    => 'Inline Skate Competition Management App'
  }
])

User.create!([
  {
    :name   => 'admin',
    :username    => 'admin',
    :password_digest => '$2a$10$RuaY60Qj0ES9Lt.dNsEmW.gLCb2tX4.cHVuXCXRFh4GCr4PuZ0gry'
  }
])